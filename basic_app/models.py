from sqlalchemy import Boolean, Column, Integer, String, Table, MetaData

# from sqlalchemy.orm import relationship

meta = MetaData()


users = Table(
    "users",
    meta,
    Column(
        "id",
        Integer,
        primary_key=True,
        index=True,
    ),
    Column("email", String, unique=True, index=True),
    Column("hashed_password", String),
    Column("is_active", Boolean, default=True),
)

items = Table(
    "items",
    meta,
    Column("id", Integer, primary_key=True, index=True),
    Column("title", String, unique=True, index=True),
    Column("description", String),
    Column("owner_id", Boolean, default=True),
)
