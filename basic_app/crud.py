from sqlalchemy.orm import Session

from . import schemas
from .models import users
from sqlalchemy import insert
from sqlalchemy.dialects import postgresql


async def create_user(tx: Session, user: schemas.UserCreate):
    fake_hashed_password = user.password + "notreallyhashed"
    q = insert(users).values(
        email=user.email,
        hashed_password=fake_hashed_password,
        is_active=True,
    )
    q = q.compile(dialect=postgresql.dialect(paramstyle="format"))

    await tx.execute(str(q), tuple(q.params.values()))
    # await tx.commit()
    return None
