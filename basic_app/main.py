from fastapi import Depends, FastAPI, HTTPException

from psycopg_pool import AsyncConnectionPool, PoolTimeout

# from sqlalchemy import create_engine  # создаем таблицы питоном

from . import crud, schemas

# from .models import meta  # создаем таблицы питоном


app = FastAPI()
DATABASE_URI = "postgresql://postgres:1234@localhost:15432/basic_app_00"
# если нужно создать таблицы
# engine = create_engine(DATABASE_URI)
# meta.create_all(bind=engine)


pool = AsyncConnectionPool(
    conninfo=DATABASE_URI,
    min_size=2,
    max_size=2,
    timeout=0.3,
)


async def get_db():
    try:
        async with pool.connection() as tx:
            yield tx
    except PoolTimeout as err:
        raise HTTPException(status_code=500, detail=str(err)) from err


@app.post("/users/")
async def create_user(user: schemas.UserCreate, tx=Depends(get_db)):

    await crud.create_user(tx=tx, user=user)

    # если раскомментировать эти строчки
    # и вызвать POST /users/ 3 раза,
    # то рейзится PoolTimeout

    # from asyncio import sleep
    # await sleep(100000)
    return 200
