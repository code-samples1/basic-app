## Старт

Начал с примера fastapi + sqlalchemy
[fastapi + sqlalchemy](https://fastapi.tiangolo.com/tutorial/sql-databases/)

---

## Зависимости
```bash
$ poetry add sqlalchemy pydantic uvicorn fastapi psycopg2-binary
```

```bash
$ poetry add -D mypy black flake8 flake8-bugbear
```

---

## Добавляем psycopg2 с ThreadedConnectionPool

Мы не используем sqlalchemy create_engine, поэтому приходится компилировать sql запрос.

Не используем, потому что aiopg, asyncpg и тп, имеют свою имплементацию этого метода. Асинхронную.

А сама алхимия будет поддерживать асинхронный create_engine в версии 2.0. Частичный функционал уже доступен, если передавать алхимии флаг future.

---

## Ставим psycopg ver3
[Дока](https://www.psycopg.org/psycopg3/docs/basic/install.html#installation)

Используется обычная установка бинарников.

Хочу отметить что другой тип установки является дефолтным для prod env [Local installation](https://www.psycopg.org/psycopg3/docs/basic/install.html#local-installation):
> This is the preferred way to install Psycopg for a production site.

```bash
$ poetry add "psycopg[binary,pool]"
```

```bash
$ poetry remove psycopg2-binary
```